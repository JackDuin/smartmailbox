/*
	Document 	: User
	Created on	: sept 22, 2016 21:46:20 PM 
	Author		: Jack Duin
	version		: 1.0
*/
package com.smartmailbox.util;

import java.io.Serializable;

public class User implements Serializable{
	
	private static final long serialVersionUID = 6297385302078200511L;
	
	private String firstname;
	private String lastname;
	private String initial;
	private String email;
	private String login =email;
	private int id;
	private String company;
	private int companyID;
	
	public User(String lastnm, String firstnm, String em, String company, String login, int i){
		this.firstname=firstnm;
		this.lastname=lastnm;
		this.id=i;
		this.company=company;
		this.login=login;
		this.email=em;
		this.initial=firstname.substring(0,1);
		
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setInitial(String initial) {
		this.initial = initial;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCompany(String Company) {
		this.company = Company;
	}

	public void setLogin(String Login) {
		this.login = Login;
	}

	public String getFirstname() {
		return firstname;
	}
	
	public String getLogin() {
		return email;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}
	public String getInitial() {
		return initial;
	}
	

	public String getCompany() {
		return company;
	}
	public int getCompanyID() {
		return companyID;
	}
	
	
	@Override
	public String toString(){
		return "Firstname="+this.firstname+", Lastname="+this.lastname+", Email="+this.email+", Country="+this.company;
	}
}