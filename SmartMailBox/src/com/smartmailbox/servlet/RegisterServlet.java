/*
	Document 	: RegisterServlet
	Created on	: sept 22, 2016 21:46:20 PM 
	Author		: Jack Duin
	version		: 1.0
*/
package com.smartmailbox.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@WebServlet(name = "Register", urlPatterns = { "/Register" })
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static Logger logger = Logger.getLogger(RegisterServlet.class);
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String login = email;
		String password = request.getParameter("password");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String company = request.getParameter("company");
		String errorMsg = null;
		if(email == null || email.equals("")){
			errorMsg = "Email ID can't be null or empty.";
		}
		if(password == null || password.equals("")){
			errorMsg = "Password can't be null or empty.";
		}
		if(firstname == null || firstname.equals("")){
			errorMsg = "Name can't be null or empty.";
		}
		if(lastname == null || lastname.equals("")){
			errorMsg = "Name can't be null or empty.";
		}
		if(company == null || company.equals("")){
			errorMsg = "Company can't be null or empty.";
		}
		
		if(errorMsg != null){
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/register.html");
			PrintWriter out= response.getWriter();
			out.println("<font color=red>"+errorMsg+"</font>");
			rd.include(request, response);
		}else{
		
		Connection con = (Connection) getServletContext().getAttribute("DBConnection");
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;

		try {
			ps1 = con.prepareStatement("insert into EMPLOYEES (FIRST_NAME, LAST_NAME, EMAIL,COMPANY) values (?,?,?,?)");
			ps1.setString(1, firstname);
			ps1.setString(2, lastname);
			ps1.setString(3, email);
			ps1.setString(4, company);
			
			ps1.execute();
		
			ps2 = con.prepareStatement("insert into EMPLOYEE_LOGIN (LOGIN,PW,EMPLOYEE_ID) values (?,?,(select ID from EMPLOYEES where EMAIL=?))");
			ps2.setString(1, login);
			ps2.setString(2, password);
			ps2.setString(3, login);
			
			ps2.execute();
			
			logger.info("User registered with email="+email);
			
			//forward to login page to login
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
			PrintWriter out= response.getWriter();
			out.println("<font color=green>Registration successful, please login below.</font>");
			rd.include(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Database connection problem");
			throw new ServletException("DB Connection problem.");
		}finally{
			try {
				ps1.close();
				ps2.close();
			} catch (SQLException e) {
				logger.error("SQLException in closing PreparedStatement");
			}
		}
		}
		
	}

}