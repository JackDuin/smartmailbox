/*
Document 	: EmployeeServlet
Created on	: sept 22, 2016 21:46:20 PM 
Author		: Jack Duin
version		: 1.0
 */

package com.smartmailbox.servlet.scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.smartmailbox.servlet.LoginServlet;
import com.smartmailbox.util.User;

@WebServlet(name = "EmployeeSearch", urlPatterns = { "/EmployeeSearch" })
public class EmployeeSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static Logger logger = Logger.getLogger(LoginServlet.class);

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String NAME = request.getParameter("val");
		if (NAME == null || NAME.trim().equals("")) {
			System.out.println("Geef de naam of de functie va de medewerker op:");
		} else {
			System.out.println(NAME);
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = (Connection) getServletContext().getAttribute(
						"DBConnection");
				PreparedStatement ps3 = con
						.prepareStatement("select * from EMPLOYEES e where e.LAST_NAME like ? or e.FIRST_NAME like ? or ROLE like ?");
				ps3.setString(1, "%" + NAME + "%");
				ps3.setString(2, "%" + NAME + "%");
				ps3.setString(3, "%" + NAME + "%");
				ResultSet rs = ps3.executeQuery();
				while (rs.next()) {
					System.out.println("<br>" + rs.getString(5) + " " + rs.getString(4)
							+ " " + rs.getString(7) + " " + rs.getBoolean(9));
				}
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
