<%--
	Document 	: SelectEmp
	Created on	: sept 22, 2016 21:46:20 PM 
	Author		: Jack Duin
	version		: 1.0
--%>
<%@ page language="java" contentType="text/html"
	pageEncoding="UTF-8"%>
<%@page import="com.smartmailbox.util.User"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% if (session.getAttribute("User") == null) { %>
<title>Insert title here</title>
</head>

<body>

<h3>U bent niet (meer) ingelogd.</h3>
<form action="Login" method="post">
<strong>Gebruikersnaam: </strong>:<input type="text" name="email"><br>
<strong>Wachtwoord: </strong>:<input type="password" name="password"><br>
<input type="submit" value="Login">
</form>
<br>
Nog geen account? registreer je hier: <a href="register.html">registreren</a>.


</body>

<% } else { %>
<%	User user = (User) session.getAttribute("User");%>
<title>Welkom <%=user.getInitial()%>. <%=user.getLastname()%></title>
<%@ include file="/WEB-INF/scripts/scriptDropDown.jspf" %>
</head>


<body>
<h3>
	Ingelogd als:
	<%=user.getInitial()%>.
	<%=user.getLastname()%></h3>
	<br>
	<br>
	<form name="vinform">
		Zoekfilter: <input type="text" name="t1" onkeyup="sendInfo()">
	</form>

	<span id="amit"> </span>
</body>

<% } %>
</html>