<%--
	Document 	: home
	Created on	: sept 22, 2016 21:46:20 PM 
	Author		: Jack Duin
	version		: 1.0
--%>

<%@page import="com.smartmailbox.util.User"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Home Page</title>
</head>
<body>
<%User user = (User) session.getAttribute("User"); %>
<h3>Hi <%= user.getInitial() %>. <%= user.getLastname() %></h3>
<strong>Your Email</strong>: <%=user.getEmail() %><br>
<strong>Your Company</strong>: <%=user.getCompany() %><br>
<br>
Ga door naar de <a href="selectemp.jsp">medewerkers.</a>
<form action="Logout" method="post">
<input type="submit" value="Logout" >
</form>
</body>
</html>